#!flask/bin/python
from app import get_app_instance
from lib.controller.root import mod_root
from lib.controller.admin import mod_admin
from lib.controller.security import mod_security
from lib.controller.homepage import mod_homepage
from lib.controller.profile import mod_profile
from lib.controller.post import mod_post
from lib.controller.event import mod_event
from lib.controller.thankyou import mod_thankyou
if __name__ == "__main__":
    #  add url in here
    app = get_app_instance()

    # Add url
    app.register_blueprint(mod_root, url_prefix='')
    app.register_blueprint(mod_security, url_prefix='/security')
    app.register_blueprint(mod_admin, url_prefix='/admin')
    app.register_blueprint(mod_homepage, url_prefix='/homepage')
    app.register_blueprint(mod_profile, url_prefix='/profile')
    app.register_blueprint(mod_post, url_prefix='/post')
    app.register_blueprint(mod_event, url_prefix='/event')
    app.register_blueprint(mod_thankyou, url_prefix='/thankyou')

    app.run(host='127.0.0.1', debug=True, port=5000)
