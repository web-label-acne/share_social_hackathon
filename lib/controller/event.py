from flask import Blueprint, current_app, jsonify
from flask import g, render_template, request, session, redirect, url_for
from app import get_db_instance
from datetime import datetime
import json

# this blueprint handles routes for the mapped URL
mod_event = Blueprint(__name__, __name__)

mongo_db = get_db_instance()


# this runs before every route in this module
@mod_event.before_request
def home_before_request():
    if 'username' not in session:
        return redirect('/security/login', code=302)


# home page (after login)
@mod_event.route('', methods=['GET'])
@mod_event.route('/', methods=['GET'])
def home_root_route():
    events = list(mongo_db.db.events.find())
    return render_template('events/index.html', title='Event Page', events=events)


@mod_event.route('/all', methods=['GET'])
def get_all_event():
    events = list(mongo_db.db.events.find())
    return jsonify(data=json.dumps(events))


@mod_event.route('/<string:id>', methods=['GET'])
def get_event_by_id(id):
    event = mongo_db.db.events.find_one({
        'id': id
    })
    items_in_event = list(mongo_db.db.items_in_event.find({
        'id_event': id
    }))
    for item_in_event in items_in_event:
        item = mongo_db.db.items.find_one({
            'id': item_in_event['id_item']
        })
        item_in_event['category'] = item['type']
    return render_template('events/detail.html', title='Event Detail Page', event=event, items=items_in_event)


@mod_event.route('/donate', methods=['GET', 'UPDATE'])
def update_items_in_event():
    if request.method == 'GET':
        return jsonify(message="OKE")
    if request.method == 'UPDATE':
        id_item_in_event = request.form['id_item_in_event']
        quatity_donate = request.form['quatity_donate']

        if not quatity_donate or not id_item_in_event:
            return jsonify(erorrs=1, message="No data")

        item_in_event = mongo_db.db.items_in_event.find_one({
            'id': id_item_in_event
        })
        item_in_event['current_quatity'] = str(int(item_in_event['current_quatity']) + int(quatity_donate))

        mongo_db.db.items_in_event.update_one({
            'id': id_item_in_event
        }, {
            '$set': {
                'current_quatity': item_in_event['current_quatity']
            }
        }, upsert=False)
        return jsonify(errors=0, message="OKE")


@mod_event.route('/calendar', methods=['GET'])
def calendar_root_route():
    events = list(mongo_db.db.events.find())
    return render_template('events/calendar.html', title='Calendar Page', events=events)
