from flask import Blueprint, current_app, jsonify
from flask import g, render_template, request, session, redirect, url_for
from app import get_db_instance
from datetime import datetime
import json

# this blueprint handles routes for the mapped URL
mod_post = Blueprint(__name__, __name__)

mongo_db = get_db_instance()


# this runs before every route in this module
@mod_post.before_request
def home_before_request():
    if 'username' not in session:
        return redirect('/security/login', code=302)


# home page (after login)
@mod_post.route('', methods=['GET'])
@mod_post.route('/', methods=['GET'])
def home_root_route():
    province = {}
    list_province = list(mongo_db.db.locations.find({}))
    for prov in list_province:
        province[prov['id']] = prov['province']
    items = {}
    list_items = list(mongo_db.db.items.find({}))
    for item in list_items:
        items[item['id']] = item['type']
    return render_template('post/index.html', title='Post Page', province=province, items=items)


@mod_post.route('/event', methods=['POST'])
def post_blog():
    try:
        type_user = request.form['type_user']
        if type_user == "1":
            id_post = str(len(mongo_db.db.posts.find().distinct('id')) + 1) 
            items = request.form['items']
            item_list = items.split(',')
            print(item_list, type(item_list))
            for index in range(0, len(item_list), 3):
                id_item = mongo_db.db.items.find_one({'type': item_list[index]})['id']
                query_item = {
                    'id_post': id_post,
                    'id_item': id_item,
                    'quatity': item_list[index + 1],
                    'description': item_list[index + 2]
                }
                mongo_db.db.items_in_post.insert_one(query_item)
            id_location = mongo_db.db.locations.find_one({'province': request.form['location']})['id']
            date_close = request.form['date_close']
            dateClose = date_close.split('-')
            query_post = {
                'username': request.form['username'], 
                'type_post': request.form['type_post'],
                'content': request.form['content'], 
                'date_post': datetime.now(), 
                'title': request.form['title'],
                'id': id_post,
                'location': id_location,
                'address': request.form['address'],
                'date_close': datetime(int(dateClose[0]), int(dateClose[1]), int(dateClose[2])),
                'status_finish': "false"
                }
            mongo_db.db.posts.insert_one(query_post)
        elif type_user == "2":
            id_post = str(len(mongo_db.db.events.find().distinct('id')) + 1) 
            items = request.form['items']
            item_list = items.split(',')
            print(item_list, type(item_list))
            for index in range(0, len(item_list), 3):
                id_item = mongo_db.db.items.find_one({'type': item_list[index]})['id']
                query_item = {
                    'id_post': id_post,
                    'id_item': id_item,
                    'quatity': item_list[index + 1],
                    'description': item_list[index + 2]
                }
                mongo_db.db.items_in_event.insert_one(query_item)
            id_location = mongo_db.db.locations.find_one({'province': request.form['location']})['id']
            date_close = request.form['date_close']
            dateClose = date_close.split('-')
            query_post = {
                'username': request.form['username'], 
                'type_post': request.form['type_post'],
                'content': request.form['content'], 
                'date_post': datetime.now(), 
                'title': request.form['title'],
                'id': id_post,
                'location': id_location,
                'address': request.form['address'],
                'date_close': datetime(int(dateClose[0]), int(dateClose[1]), int(dateClose[2])),
                'status_finish': "false"
                }
            mongo_db.db.events.insert_one(query_post)
        return jsonify(errors=0, message='OK',id_post=id_post)
    except Exception as e:
        raise e


@mod_post.route('/new_item', methods=['POST'])
def post_item():
    try:
        id = len(mongo_db.db.items.find().distinct('id')) + 1
        query_item = {
            'id': id,
            'type': request.form['type'],
        }
        mongo_db.db.items.insert(query_item)
        return jsonify(errors=0, message='OK')
    except Exception as e:
        raise e


@mod_post.route('/<string:id>', methods=['GET'])
def get_post(id):
    post = mongo_db.db.posts.find({'id': id})
    l_post = list(post)[0]
    l_post.pop('_id', None)
    return jsonify(errors=0, message="OK", data=l_post)
