from flask import Blueprint, current_app, jsonify, redirect
from flask import Flask, render_template, request, redirect, session, flash
from app import get_db_instance
from datetime import datetime

# this blueprint handles routes for the mapped URL
mod_profile = Blueprint(__name__, __name__)


@mod_profile.route('/', methods=['GET'])
def profile_route():
    mongo = get_db_instance()
    users = mongo.db.accounts
    posts = mongo.db.posts
    user = users.find_one({
        'username': session['username']
    })
    current_user = session['username']

    post = posts.find({
        'username': session['username']
    })


    # post = posts.find({
    #     'username': {"$in": ["tham", "tntteam", "thang"]}
    # })
    post = list(post)
    for index in range(len(post)):
        matching_person = list(mongo.db.user_in_post.find({"id_post": post[index]["id"]}))
        if len(matching_person) != 0:
            post[index]["matching_person"] = []
            for person in matching_person:
                post[index]["matching_person"].append(person["matching_person"])
    return render_template("profile/index.html", user=user, post=post, current_user = current_user)

@mod_profile.route('/<username>', methods = ['GET'])
def profile_each(username):
    mongo = get_db_instance()
    users = mongo.db.accounts
    posts = mongo.db.posts
    user = users.find_one({
        'username': username
    })

    post = posts.find({
        'username': username
    })

    current_user = session["username"]

    # post = posts.find({
    #     'username': {"$in": ["tham", "tntteam", "thang"]}
    # })
    post = list(post)
    for index in range(len(post)):
        matching_person = list(mongo.db.user_in_post.find({"id_post": post[index]["id"]}))
        if len(matching_person) != 0:
            post[index]["matching_person"] = []
            for person in matching_person:
                post[index]["matching_person"].append(person["matching_person"])
    return render_template("profile/index.html", user = user, post = post, current_user = current_user)
@mod_profile.route('/thank_you', methods = ['POST'])
def thank_you():
    mongo = get_db_instance()
    thank_you_letters = mongo.db.thank_you_letters
    thank_you_letters.insert({
        "fk_user_send": session["username"],
        "message": request.form["message"],
        "time_send": datetime.now(),
        "fk_user_receive": request.form["receiver"]
    })

    return jsonify(errors=0, message='Successful')
@mod_profile.route('/create', methods = ['POST'])
def create():
    mongo = get_db_instance()
    if 'profile_image' in request.files:
        users = mongo.db.accounts
        user = users.find_one({
            'username': session["username"]
        })
        profile_image = request.files['profile_image']
        mongo.save_file(profile_image.filename, profile_image)
        mongo.db.accounts.update_one({'username': session["username"]}, { "$set": {'profile_image_name': profile_image.filename}})
    return redirect('/profile')
@mod_profile.route('/file/<filename>')
def file(filename):
    mongo = get_db_instance()
    return mongo.send_file(filename)

