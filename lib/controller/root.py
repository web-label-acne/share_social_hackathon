from flask import Blueprint, current_app
from flask import Flask, render_template, request, redirect, session, flash

# this blueprint handles routes for the mapped URL
mod_root = Blueprint(__name__, __name__)


@mod_root.route('/', methods=['GET'])
def root_route():
    if 'username' not in session:
        return redirect('/homepage', code=302)
    else:
        return redirect('/security/login', code=302)


@mod_root.route('/favicon.ico', methods=['GET'])
def root_favicon():
    return redirect('/static/images/favicon.png')


