from flask import Blueprint, jsonify, g, render_template, request, session, redirect, Response, current_app, send_file, \
    make_response, stream_with_context, url_for, current_app
from app import get_db_instance
import dateutil.parser
from datetime import datetime

# this blueprint handles routes for the mapped URL
mod_homepage = Blueprint(__name__, __name__)
mongo = get_db_instance()


# this runs before every route in this module
@mod_homepage.before_request
def home_before_request():
    if 'username' not in session:
        return redirect('/security/login', code=302)


# home page (after login)
@mod_homepage.route('', methods=['GET'])
@mod_homepage.route('/', methods=['GET'])
def home_root_route():
    province = {}
    list_province = list(mongo.db.locations.find({}))
    for prov in list_province:
        province[prov['id']] = prov['province']
    items = {}
    list_items = list(mongo.db.items.find({}))
    for item in list_items:
        items[item['id']] = item['type']
    post_username = {}
    if ('typeNewfeed' not in session) or (session['typeNewfeed'] == ""):
        session['typeNewfeed'] = 'All'
    if ('typeLocation' not in session) or (session['typeLocation'] == ""):
        session['typeLocation'] = 'All'
    if ('fromDate' not in session) or (session['fromDate'] == ""):
        session['fromDate'] = ''
    if ('toDate' not in session) or (session['toDate'] == ""):
        session['toDate'] = ''
    typeNewfeed = session['typeNewfeed']
    typeLocation = session['typeLocation']
    fromDate = session['fromDate']
    toDate = session['toDate']
    query = {}
    events = {}
    list_event = list(mongo.db.events.find({}))
    for eve in list_event:
        events[eve['title']] = eve['address']
    if typeNewfeed != 'All':
        if typeNewfeed == "Giving":
            typeNewfeed = 0
        elif typeNewfeed == "Receving":
            typeNewfeed = 1
        query['type_post'] = str(typeNewfeed)
    if typeLocation != "All":
        id_location = mongo.db.locations.find_one({'province': typeLocation})['id']
        query['location'] = id_location
    if len(fromDate) != 0 and len(toDate) == 0:
        from_date = fromDate.split('-')
        query['date_post'] = {'$gte': datetime(int(from_date[0]), int(from_date[1]), int(from_date[2]))}
    elif len(fromDate) == 0 and len(toDate) != 0:
        to_date = toDate.split('-')
        query['date_post'] = {'$lte': datetime(int(to_date[0]), int(to_date[1]), int(to_date[2]))}
    elif len(fromDate) != 0 and len(toDate) != 0:
        from_date = fromDate.split('-')
        query['date_post'] = {'$gte': datetime(int(from_date[0]), int(from_date[1]), int(from_date[2])),
                              '$lte': datetime(int(to_date[0]), int(to_date[1]), int(to_date[2]))}
    posts = list(mongo.db.posts.find(query).limit(100))
    list_item = list(mongo.db.items_in_post.find({}))
    for post in posts:
        # query profile_image_name
        user_post_name = post["username"]
        user_post = mongo.db.accounts.find_one({"username": user_post_name})

        if "profile_image_name" in user_post.keys():
            post["profile_image_name"] = user_post["profile_image_name"]
            print(user_post["profile_image_name"])
        id = post['id']
        list_item = list(mongo.db.items_in_post.find({'id_post': id}, {'_id': 0}))
        post['list_item'] = list_item
        list_comment = list(mongo.db.comments_in_post.find({'id_post': id}, {'_id': 0, 'id_post': 0}))

        # get num_giving and num_receiving for show
        for comment in list_comment:
            user = mongo.db.accounts.find_one({
                'username': comment['username']
            })
            if "profile_image_name" in user.keys():
                comment["profile_image_name"] = user["profile_image_name"]
            comment['num_giving'] = user['num_giving']
            comment['num_receiving'] = user['num_receiving']

        post['list_comment'] = list_comment
        post_username[post['id']] = post['username']

        # get num_giving and num_receiving for show
        user = mongo.db.accounts.find_one({
            'username': post['username']
        })
        post['num_giving'] = user['num_giving']
        post['num_receiving'] = user['num_receiving']

    # print(posts)
    return render_template('homepage/index.html', title='Home Page', province=province, posts=posts, items=items,
                           post_username=post_username, events=events)


@mod_homepage.route('/filter', methods=['POST'])
def change_session_type_newfeed():
    type_newfeed = request.form['typeNewfeed']
    typeLocation = request.form['typeLocation']
    fromDate = request.form['fromDate']
    toDate = request.form['toDate']
    session['typeNewfeed'] = type_newfeed
    session['typeLocation'] = typeLocation
    session['fromDate'] = fromDate
    session['toDate'] = toDate
    return jsonify(errors=0, message='Changed type newfeed success!')


@mod_homepage.route('/addComment', methods=['POST'])
def add_Comment():
    id_post = request.form['id_post']
    content = request.form['content']
    username = request.form['username']
    print(id_post, username)
    mongo.db.comments_in_post.insert({
        'id_post': id_post,
        'username': username,
        'content': content
    })
    # user_post = mongo.db.posts.find_one({'id': id_post})['username']
    return jsonify(errors=0, message='Add comment success!')

@mod_homepage.route('/confirm_comment', methods=['POST'])
def confirm_comment():
    id_post = request.form['id_post']
    matching_person = mongo.db.user_in_post.find_one({'id_post': id_post})['matching_person']
    print(id_post)
    if not matching_person:
        matching_person = request.form['matching_person']
        mongo.db.user_in_post.insert({
            'id_post': id_post,
            'matching_person': matching_person
        })
        return jsonify(errors=0, message="Congratulation! This comment is confirmed successfully!")
    else:
        return jsonify(errors=1, message="Sorry but this post is confirmed!")

