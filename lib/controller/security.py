from flask import Blueprint, jsonify, g, render_template, request, session, redirect, Response, current_app, send_file, \
    make_response, stream_with_context, url_for
from app import get_db_instance, get_app_instance
# this blueprint handles routes for the mapped URL
mod_security = Blueprint(__name__, __name__)
import json


@mod_security.route("/")
def security_root():
    return redirect('/security/login')


@mod_security.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('security/login.html', title='Login')
    elif request.method == 'POST':
        mongo = get_db_instance()
        users = mongo.db.accounts
        login_user = users.find_one({
            'username': request.form['username']
        })

        if login_user:
            if request.form['password'] == login_user['password']:
                session['username'] = request.form['username']
                session['type_user'] = login_user['type_user']
                session['num_giving'] = login_user['num_giving']
                session['num_receiving'] = login_user['num_receiving']
                type_user = login_user['type_user']
                return jsonify(errors=0, type_user=type_user)
        return jsonify(errors=1, message="Username or password is incorrect")


@mod_security.route('/register', methods=['POST', 'GET'])
def register():
    mongo = get_db_instance()
    list_province = list(mongo.db.locations.distinct('province'))
    if request.method == 'POST':
        accounts = mongo.db.accounts
        existing_user = accounts.find_one({
            'username': request.form['username'],
        })
        if existing_user is None:
            if request.form['password'] != request.form['repassword']:
                return render_template('security/register.html', title='Register Page', province=list_province)
            username = request.form['username']
            password = request.form['password']
            typ = str(request.form['type'])
            location = request.form['location']
            tel = request.form['tel']
            mail = request.form['mail']
            if not(len(username) == 0 or len(password) == 0 or len(typ) == 0 or len(location) == 0 or len(tel) == 0 or len(mail) == 0):
                accounts.insert({
                    'username': request.form['username'],
                    'password': request.form['password'],
                    'type_user': str(request.form['type']),
                    'location': request.form['location'],
                    'tel': request.form['tel'],
                    'mail': request.form['mail']
                })
                session['username'] = request.form['username']
            else:
                return jsonify(errors=1, message='Please fill out fully!')
            return jsonify(errors=0, message=' is created successfully')
        return jsonify(errors=1, message=' is existed')
    return render_template('security/register.html', title='Register Page', province=list_province)


@mod_security.route('/logout', methods=['GET'])
def logout():
    keys = ['name', 'username', 'uid', 'type_user']
    for keyname in keys:
        if keyname in session:
            del session[keyname]
    return render_template("security/logout.html")
