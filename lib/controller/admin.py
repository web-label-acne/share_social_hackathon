from flask import Blueprint, jsonify, g, render_template, request, session, redirect, Response, current_app, send_file, \
    make_response, stream_with_context, url_for
from app import get_db_instance, get_app_instance
# this blueprint handles routes for the mapped URL
mod_admin = Blueprint(__name__, __name__)
import json

@mod_admin.route('', methods=['GET', 'POST'])
@mod_admin.route('/', methods=['GET', 'POST'])
def admin():
    mongo = get_db_instance()
    if request.method == "GET":
        labelTable = list(mongo.db.label_image.find({}))
        total = mongo.db.label_image.count_documents({})
        listUID = mongo.db.label_image.distinct('uid')
        listUsername = mongo.db.label_image.distinct('username')
        return render_template('admin/index.html', title='admin Page', labelTable=labelTable, total=total, listUID=listUID, listUsername=listUsername)
    elif request.method == "POST":
        delList = request.form.to_dict(flat=False)['delList[]']
        for delEle in delList:
            name_image = mongo.db.label_image.find_one({'jid': int(delEle)})['name_image']
            mongo.db.label_image.delete_many({'jid': int(delEle)})
            list_image = list(mongo.db.label_image.find({'name_image': name_image}))
            if len(list_image) == 0:
                id_image = mongo.db.fs.files.find_one({'filename': name_image})['_id']
                mongo.db.fs.files.delete_many({'filename': name_image})
                mongo.db.fs.chunks.delete_many({'files_id': id_image})
        return jsonify(message='Deleted ' + str(len(delList)) + ' rows!', error=0)