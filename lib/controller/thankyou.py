from flask import Blueprint, current_app, jsonify
from flask import Flask, render_template, request, redirect, session, flash
from app import get_db_instance
from datetime import datetime
# this blueprint handles routes for the mapped URL
mod_thankyou = Blueprint(__name__, __name__)

@mod_thankyou.route('/', methods=['GET'])
def thankyou_route():
    mongo = get_db_instance()
    users = mongo.db.accounts
    letters = mongo.db.thank_you_letters
    user = users.find_one({
        'username': session['username']
    })

    letters = letters.find({
        'fk_user_receive': session['username']
    })
    letters = list(letters)
    return render_template("profile/thankyou.html", letters = letters, user=user)
