#!flask/bin/python
import os, flask, yaml, sys
from flask import Blueprint
import signal
import lib.model
from flask_moment import Moment


def create_app(debug=None):  # using app factory - see http://flask.pocoo.org/docs/patterns/appfactories/

    app = flask.Flask(__name__, template_folder="templates", static_folder='')
    moment = Moment(app)
    app.config.from_object('lib.config')

    # database initialization
    db = lib.model.db  # the db instance that our models package created

    stream = open(os.path.abspath(os.path.dirname(__file__)) + "/lib/controller/_mappings.yml", "r")
    modules = yaml.load(stream, Loader=yaml.SafeLoader)
    for module in modules:
        # Load each module
        modname = module
        loaded_mod = __import__('lib.controller.' + modname, None, None, modname)
        for obj in vars(loaded_mod).values():
            if isinstance(obj, Blueprint):
                app.register_blueprint(obj, url_prefix=modules[module]['url'])
    stream.close()

    # Wrap the Api with swagger.docs. It is a thin wrapper around the Api class that adds some swagger smarts
    # Swagger(application)

    # initialize database
    db.init_app(app)
    # signal.signal(signal.SIGINT, signal_handler)
    return app


# utility method so that blueprint modules can get the db instance
def get_db_instance():
    return lib.model.db


def scheduler_stop():
    try:
        app.scheduler.shutdown()
    except:
        pass


# this runs when control-c is pressed.  maybe also when the app is shut down ?
def signal_handler(signal, frame):
    scheduler_stop()
    sys.exit(0)


# utility method so that other scripts can get the app instance
def get_app_instance():
    return app


# wsgi uses this object; to run the server local, use run.py
app = create_app()

