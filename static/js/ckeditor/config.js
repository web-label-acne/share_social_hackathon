/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    config.uiColor = '#AADC6E';
    // config.removeButtons = 'Subscript,Superscript,Source,Save,NewPage,Preview,Print,Flash,Templates,Find,Replace,' +
    //     'SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,' +
    //     'HiddenField,Strike,CopyFormatting,RemoveFormat,Anchor,Language,Unlink,Image,Table,Smiley,' +
    //     'Iframe,Maximize,Link,ShowBlocks,PasteFromWord,PasteText,Paste,Blockquote,Indent,Outdent,' +
    //     'CreateDiv,bidi,PageBreak,spellchecker,BidiRtl,BidiLtr,JustifyBlock,SpecialChar,About,' +
    //     'JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock';
    config.removePlugins = 'elementspath';
    config.codeSnippet_codeClass = 'myCustomClass';
};

